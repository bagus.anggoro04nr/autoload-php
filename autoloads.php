<?php
namespace autoload;

class autoloads
{
    private $fileName;
    private $pathClass;
    private $pathFound;
    private $nmSpc;
    private $hostName;
    private $paths;
    
    public function __construct($namehost=null, $namespace= null, $path = null){
        $this->hostName = $namehost;
        $this->nmSpc = $namespace;
        $this->pathClass = $path;
        $this->fileName = '';
        $this->pathFound = false;
        $this->paths = "";
    }
    
    public function register_spl(){
        return spl_autoload_register(array($this, 'loadFileClass'));
    }
    
    public function unregister_spl(){
        return spl_autoload_unregister(array($this, 'loadFileClass'));
    }
    
    
    private function loadFileClass($className)
    {
        
       $this->pathFound = $this->requireFileArray();  
       return $this->pathFound;
    }
    
    private function requireFileArray(){
        
        foreach ($this->pathClass as $pathFile){
            

            $files  =   glob($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $this->hostName . DIRECTORY_SEPARATOR .
                $this->nmSpc . DIRECTORY_SEPARATOR . $pathFile);
            
          
            if(empty($files)) {
                $this->pathFound = false;
            }
            
            foreach (array_filter($files, 'is_file') as $file) {
                
                if(file_exists($file) ) {
                    $this->paths = stream_resolve_include_path($file);
//                     echo $this->paths . " </br>";
                    require_once $this->paths;
                    $this->pathFound = true;
                }else{
                    throw new \Exception("Error Processing Request" ,  1);
                }
            }
        }
        
        return $this->pathFound;
        
    }

}

// ini untuk hostname yg di pakai.
$hostname = "myapp_one"; 

// tempat folder penyimpanan class bisa berupa  -- folder vendor -- atau yang lainnya
$namespace = "autoload"; 

// folder untuk multiple class dalam one folder yang di load bisa berupa -- folder src -- atau yang lainnya
$folderfileclass = array('sessions/*', 'URL/*', 'inits/*', 'pages/*', 'api/*', 'camera/*', 'po/*'); 

$autoloadclass = new autoloads($hostname, $namespace, $folderfileclass);
$autoloadclass->register_spl();

// Note : import with require_once di file index.php atau file untuk menghubungkan proses... example -- routers / boot --
// Thanks to Google, Stack Overflow for clue and another...


